#define INPUT_L A0
#define INPUT_ML A1
#define INPUT_MR A2
#define INPUT_R A3

#define FLOOR_L A4
#define FLOOR_R A5

#define MOTOR_R_A 6
#define MOTOR_R_B 9
#define MOTOR_L_A 3
#define MOTOR_L_B 5

#define R 1
#define L -1

void setup() {
  Serial.begin(115200);
  // Setup pins:
  pinMode(MOTOR_R_A,OUTPUT);
  pinMode(MOTOR_R_B,OUTPUT);
  pinMode(MOTOR_L_A,OUTPUT);
  pinMode(MOTOR_L_B,OUTPUT);
  
  pinMode(INPUT_L,INPUT);
  pinMode(INPUT_ML,INPUT);
  pinMode(INPUT_MR,INPUT);
  pinMode(INPUT_R,INPUT);
  pinMode(FLOOR_L,INPUT);
  pinMode(FLOOR_R,INPUT);
  
  delay(5000);  
}

boolean sensor_1=false;
boolean sensor_2=false;
boolean sensor_3=false;
boolean sensor_4=false;
boolean floor_l=false;
boolean floor_r=false;

int last_seen=0;

// This function reads the sensor values and prints the results
void read_inputs(void){
  sensor_1=!digitalRead(INPUT_L);
  sensor_2=!digitalRead(INPUT_ML);
  sensor_3=!digitalRead(INPUT_MR);
  sensor_4=!digitalRead(INPUT_R);
  floor_r=digitalRead(FLOOR_R);
  floor_l=digitalRead(FLOOR_L);  
  Serial.print("S1: ");
  Serial.print(sensor_1);
  Serial.print(" - S2: ");
  Serial.print(sensor_2);
  Serial.print(" - S3: ");
  Serial.print(sensor_3);
  Serial.print(" - S4: ");
  Serial.print(sensor_4);
  Serial.print(" - FL: ");
  Serial.print(floor_l);
  Serial.print(" - FR: ");
  Serial.println(floor_r);
}

void loop() {
  // put your main code here, to run repeatedly:
  read_inputs();

  
  if (floor_r==true){
    rotate_L(100,500);
  }else if (floor_l==true){
    rotate_R(100,500);
  }else{
    if (sensor_1==true && sensor_2==false && sensor_3==false && sensor_4 ==false){
      rotate_L(100,0);
      last_seen=L;
    }else if(sensor_1==false && sensor_2==false&& sensor_3==false&& sensor_4 ==true){
      rotate_R(100,0);
      last_seen=R;
    }else if(sensor_1==false && sensor_2==true && sensor_3==false && sensor_4 ==false){
      turn_L(100,0);
      last_seen=L;
    }else if(sensor_1==false && sensor_2==false && sensor_3==true && sensor_4 ==false){
      turn_R(100,0);
      last_seen=R;
    }else if(sensor_1==false && sensor_2==true && sensor_3==true && sensor_4 ==false){
      move_forward(100,0);
    }else{
      if (last_seen==R){
        rotate_R(100,0);
      }else{
        rotate_L(100,0);
      }
    }
  }
  
}

// Move backwards - > Wheel_speed - from 0 to 100. -> time_moving in ms  (1sec=1000ms)
void move_backwards(float wheel_speed,int time_moving ){
  motor_R(-1,wheel_speed);
  motor_L(-1,wheel_speed);
  delay(time_moving);
}

// Move forward - > Wheel_speed - from 0 to 100. -> time_moving in ms  (1sec=1000ms)
void move_forward(float wheel_speed,int time_moving ){
  motor_R(1,wheel_speed);
  motor_L(1,wheel_speed);
  delay(time_moving);
}

// Turn right (Only Left wheel turns) - > Wheel_speed - from 0 to 100. -> time_moving in ms  (1sec=1000ms)
void turn_R(float wheel_speed,int time_moving ){
  motor_R(0,wheel_speed);
  motor_L(1,wheel_speed);
  delay(time_moving);
}

// Turn right (Only Rigth wheel turns) - > Wheel_speed - from 0 to 100. -> time_moving in ms  (1sec=1000ms)
void turn_L(float wheel_speed,int time_moving ){
  motor_R(1,wheel_speed);
  motor_L(0,wheel_speed);
  delay(time_moving);
}

// Rotate left (Left wheel turns backwards and right wheel forward) - > Wheel_speed - from 0 to 100. -> time_moving in ms  (1sec=1000ms)
void rotate_L(float wheel_speed,int time_moving ){
  motor_R(1,wheel_speed);
  motor_L(-1,wheel_speed);
  delay(time_moving);
}

// Rotate Right (Right wheel turns backwards and Left wheel forward) - > Wheel_speed - from 0 to 100. -> time_moving in ms  (1sec=1000ms)
void rotate_R(float wheel_speed,int time_moving ){
  motor_R(-1,wheel_speed);
  motor_L(1,wheel_speed);
  delay(time_moving);
}

// Control of Right wheel => direction_to_move (-1=backwards,0=No turn,1=Forward) / Speed= from 0 to 100
void motor_R(int direction_to_move, float rot_speed){
  unsigned char ucspeed=(rot_speed/100*255);
  if (direction_to_move==-1){
    digitalWrite(MOTOR_R_A,LOW);
    analogWrite(MOTOR_R_B,ucspeed);
  }else if (direction_to_move==1){
    analogWrite(MOTOR_R_A,ucspeed);
    digitalWrite(MOTOR_R_B,LOW);
  }else{
    digitalWrite(MOTOR_R_A,LOW);
    digitalWrite(MOTOR_R_B,LOW);
  }
}


// Control of Left wheel => direction_to_move (-1=backwards,0=No turn,1=Forward) / Speed= from 0 to 100
void motor_L(int direction_to_move, float rot_speed){
  unsigned char ucspeed=(rot_speed/100*255);
  if (direction_to_move==-1){
    digitalWrite(MOTOR_L_A,LOW);
    analogWrite(MOTOR_L_B,ucspeed);
  }else if (direction_to_move==1){
    analogWrite(MOTOR_L_A,ucspeed);
    digitalWrite(MOTOR_L_B,LOW);
  }else{
    digitalWrite(MOTOR_L_A,LOW);
    digitalWrite(MOTOR_L_B,LOW);
  }
}
